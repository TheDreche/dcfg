# dcfg

A power-user way of configuring applications.

## What does this mean?

Once this project is finished (version 1.x.x), it will contain:
- A file type specification describing a powerful and extendable
  file type for configuration files.
- Best practices on how to structure configuration options.
- A library with interfaces for many programming languages to read
  the specified file type.

For non-power-users, some more basic file types will also be supported
reading by the library (json, toml). This and the interfaces to the
library in many programming languages may be added way after finishing
version 1.0.0.

## Versioning

This project uses [semantic versioning](https://semver.org).

// vim:filetype=markdown:
